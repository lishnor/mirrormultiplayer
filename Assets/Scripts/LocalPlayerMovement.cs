﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PhysicsLink))]
public class LocalPlayerMovement : NetworkBehaviour
{
    [SyncVar]
    Vector2 movement;

    [SyncVar]
    float speed = 5f;

    PhysicsLink physics;

    private void Start()
    {
        physics = GetComponent<PhysicsLink>();
    }

    void Update()
    {
        if (hasAuthority)
        {
            GetInput();
        }
    }

    private void FixedUpdate()
    {
        Move();
    }

    private void GetInput() 
    {
        movement.x = Input.GetAxis("Horizontal");
        movement.y = Input.GetAxis("Vertical");
    }

    private void Move()
    {
        if (movement.magnitude > 0.1f) 
        {
            Vector3 targetVelocity = new Vector3(movement.x, 0, movement.y) * speed;
            targetVelocity.y = physics.Velocity.y;
            physics.ChangeVelocity(targetVelocity);
        }
    }
}
