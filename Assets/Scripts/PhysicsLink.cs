﻿using UnityEngine;
using Mirror;
using System.Collections;
using System;

[RequireComponent(typeof(Rigidbody))]
public class PhysicsLink : NetworkBehaviour
{
    Rigidbody rb;

    [SyncVar]
    Vector3 velocity;
    public Vector3 Velocity => velocity;
    [SyncVar]
    Quaternion rotation;
    public Quaternion Rotation => rotation;
    [SyncVar]
    Vector3 position;
    public Vector3 Position => position;
    [SyncVar]
    Vector3 angularVelocity;
    public Vector3 AngularVelocity => angularVelocity;


    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (isServer)
        {
            SetServerRb();
        }
        if (isClient)
        {
            SetClientRb();
        }
    }
    [Command]
    public void CmdResetPose()
    {
        rb.position = Vector3.up;
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
    }

    public void ApplyForce(Vector3 force, ForceMode FMode)
    {
        rb.AddForce(force, FMode);
        CmdApplyForce(force, FMode);
    }

    public void ChangeVelocity(Vector3 velocity)
    {
        rb.velocity = velocity;
        CmdChangeVelocity(velocity);
    }
    
    [Command]
    public void CmdChangeVelocity(Vector3 velocity)
    {
        rb.velocity = velocity;
    }

    [Command]
    public void CmdApplyForce(Vector3 force, ForceMode FMode)
    {
        rb.AddForce(force, FMode);
    }

    void SetServerRb() 
    {
        position = rb.position;
        rotation = rb.rotation;
        velocity = rb.velocity;
        angularVelocity = rb.angularVelocity;
        rb.position = position;
        rb.rotation = rotation;
        rb.velocity = velocity;
        rb.angularVelocity = angularVelocity;
    }

    void SetClientRb()
    {
        rb.position = position + velocity * (float)NetworkTime.rtt;
        rb.rotation = rotation * Quaternion.Euler(angularVelocity * (float)NetworkTime.rtt);
        rb.velocity = velocity;
        rb.angularVelocity = angularVelocity;
    }
}